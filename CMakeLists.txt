cmake_minimum_required(VERSION 3.5)
project(RubikGL)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

include_directories(externals/include
        externals/gl3w/include
        externals/glfw3/include
        externals/glm
        externals/tinyobjloader
        externals/AntTweakBar/include)

add_subdirectory(externals/AntTweakBar)
add_subdirectory(externals/gl3w)
add_subdirectory(externals/glfw3)

file(COPY src/assets DESTINATION ${CMAKE_BINARY_DIR})
file(COPY src/shaders DESTINATION ${CMAKE_BINARY_DIR})

file(GLOB RUBIKGL_SOURCES "src/*.h" "src/*.cpp")

set(SOURCE_FILES ${RUBIKGL_SOURCES})

add_executable(RubikGL ${SOURCE_FILES})
if(UNIX)
    target_link_libraries(RubikGL AntTweakBar gl3w glfw GL dl X11 Xi Xrandr Xxf86vm Xinerama Xcursor rt m pthread )
elseif(WIN32)
    target_link_libraries(RubikGL AntTweakBar gl3w glfw opengl32)
    add_custom_command(TARGET RubikGL POST_BUILD
	    COMMAND ${CMAKE_COMMAND} -E copy_if_different
	    $<TARGET_FILE:AntTweakBar>
	    $<TARGET_FILE_DIR:RubikGL>)
endif()
