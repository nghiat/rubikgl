#pragma once

#include "ObjPiece.h"
#include <glm/glm.hpp>
#include <vector>

#define CENTERNUM 1
#define CORNERNUM 8
#define EDGENUM 12
#define SIDENUM 6
#define TOTALPIECES 27

#define PI 3.14159265359f

struct FullPiece{
	ObjPiece piece;
	std::vector<ObjPiece> stickers;
};

extern ObjPiece rubikCenter;
extern ObjPiece rubikCorner;
extern ObjPiece rubikEdge;
extern ObjPiece rubikSide;
extern ObjPiece rubikSticker;

extern ObjPiece center[CENTERNUM];
extern ObjPiece corner[CORNERNUM];
extern ObjPiece edge[EDGENUM];
extern ObjPiece side[SIDENUM];
extern ObjPiece stickers[TOTALPIECES * 2];
//All above pieces in a more proper order
extern FullPiece pieces[TOTALPIECES];
//List of all 9 pieces for each swipes
//And there are 9 ways of swiping the rubik
extern int swipe[9][9];
//rotation axis for each swipe
extern glm::vec3 axis[9];

extern int face[9][2];
//Create new piece by translating and rotating the original piece
ObjPiece transform(const ObjPiece& originalPiece, glm::vec3 translateVector, float angle, glm::vec3 v);

void loadBasePieces();
void loadCenters();
void loadCorners();
void loadEdges();
void loadSides();
void loadStickers();
void loadPieces();
