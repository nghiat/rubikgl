#include "HDRImage.h"
#include "hdrloader.h"
#include <vector>
#include <memory.h>

GLuint convertCrossToCubemap(const char* filename){
	GLuint tex;
//	ILuint hdr_image;
	std::vector<GLubyte*> imageData;
	GLubyte* temp;
	int width, height;
	int elementSize = 12;
	int fWidth, fHeight;
	HDRLoaderResult result;
	HDRLoader::load(filename, result);
	temp = (GLubyte*)result.cols;
	width = result.width;
	height = result.height;

//	ilGenImages(1, &hdr_image);
//	ilBindImage(hdr_image);
//	ilLoadImage(filename);
	//get the source data
//	temp = ilGetData();
//	width = ilGetInteger(IL_IMAGE_WIDTH);
//	height = ilGetInteger(IL_IMAGE_HEIGHT);

	unsigned int lineSize;
	lineSize = elementSize * width;
	unsigned int sliceSize = lineSize * height;

	GLubyte *tempBuf = new GLubyte[lineSize];
	GLubyte *top = temp;
	GLubyte *bottom = top + (sliceSize - lineSize);

	for (int jj = 0; jj < (height >> 1); jj++) {
		memcpy(tempBuf, top, lineSize);
		memcpy(top, bottom, lineSize);
		memcpy(bottom, tempBuf, lineSize);

		top += lineSize;
		bottom -= lineSize;
	}

	fWidth = width / 3;
	fHeight = height / 4;

	GLubyte *face = new GLubyte[fWidth * fHeight * elementSize];
	GLubyte *ptr;

	//extract the faces

	// positive X
	ptr = face;
	for (int j = 0; j < fHeight; j++) {
		memcpy(ptr, &temp[((height - (fHeight + j + 1))*width + 2 * fWidth) * elementSize], fWidth*elementSize);
		ptr += fWidth*elementSize;
	}
	imageData.push_back(face);

	// negative X
	face = new GLubyte[fWidth * fHeight * elementSize];
	ptr = face;
	for (int j = 0; j < fHeight; j++) {
		memcpy(ptr, &temp[(height - (fHeight + j + 1))*width*elementSize], fWidth*elementSize);
		ptr += fWidth*elementSize;
	}
	imageData.push_back(face);

	// positive Y
	face = new GLubyte[fWidth * fHeight * elementSize];
	ptr = face;
	for (int j = 0; j < fHeight; j++) {
		memcpy(ptr, &temp[((4 * fHeight - j - 1)*width + fWidth)*elementSize], fWidth*elementSize);
		ptr += fWidth*elementSize;
	}
	imageData.push_back(face);

	// negative Y
	face = new GLubyte[fWidth * fHeight * elementSize];
	ptr = face;
	for (int j = 0; j < fHeight; j++) {
		memcpy(ptr, &temp[((2 * fHeight - j - 1)*width + fWidth)*elementSize], fWidth*elementSize);
		ptr += fWidth*elementSize;
	}
	imageData.push_back(face);

	// positive Z
	face = new GLubyte[fWidth * fHeight * elementSize];
	ptr = face;
	for (int j = 0; j < fHeight; j++) {
		memcpy(ptr, &temp[((height - (fHeight + j + 1))*width + fWidth) * elementSize], fWidth*elementSize);
		ptr += fWidth*elementSize;
	}
	imageData.push_back(face);

	// negative Z
	face = new GLubyte[fWidth * fHeight * elementSize];
	ptr = face;
	for (int j = 0; j < fHeight; j++) {
		for (int i = 0; i < fWidth; i++) {
			memcpy(ptr, &temp[(j*width + 2 * fWidth - (i + 1))*elementSize], elementSize);
			ptr += elementSize;
		}
	}
	imageData.push_back(face);

	width = fWidth;
	height = fHeight;

	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	for (int i = 0; i < 6; i++) {
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
			GL_RGBA32F, width, height, 0,
			GL_RGB, GL_FLOAT, imageData[i]);
	}
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	return tex;
}

