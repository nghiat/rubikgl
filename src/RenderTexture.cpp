#include "RenderTexture.h"


RenderTexture::RenderTexture()
{
	tWidth = 0;
	tHeight = 0;
}

RenderTexture::~RenderTexture()
{
}

void RenderTexture::Init(int width, int height, int depth){
	glGenFramebuffers(1, &fbo);
	glGenTextures(1, &fto);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glBindTexture(GL_TEXTURE_2D, fto);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, GL_FALSE, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fto, 0);

	if (depth == 24){
		glGenRenderbuffers(1, &fdo);
		glBindRenderbuffer(GL_RENDERBUFFER, fdo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, fdo);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	tWidth = width;
	tHeight = height;
}

void RenderTexture::resizeFB(int w, int h){
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glBindTexture(GL_TEXTURE_2D, fto);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, w, h, GL_FALSE, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fto, 0);
	if (glIsRenderbuffer(fdo)){
		glBindRenderbuffer(GL_RENDERBUFFER, fdo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, w, h);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, fdo);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderTexture::bindFB(){
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glViewport(0, 0, tWidth, tHeight);
}

void RenderTexture::bind(){
	glBindTexture(GL_TEXTURE_2D, fto);
}

void RenderTexture::unBind(){
	glBindTexture(GL_TEXTURE_2D, 0);
}

int RenderTexture::getWidth(){
	return tWidth;
}

int RenderTexture::getHeight(){
	return tHeight;
}