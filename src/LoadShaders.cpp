//Created by Nghia Tran and the fix is for his stupid computer
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#define GLEW_STATIC
#include <GL/gl3w.h>
#include "LoadShaders.h"

using namespace std;


//If you are using Intel HD graphic card that only supports OpenGL 3.1 and GLSL 1.30 which can't use layout attribute in shaders
//this function will delete the layout attribute in the shaders and use the old glBindAttribLocation()
void format(string &input, vector<int> &location, vector<string> &name){
	if (input.length() == 0) return;
	bool version = true;
	if (input[0] == ' '){
		for (unsigned i = 0; i < input.length(); i++)
			if (input[i] == ' '){
				input.erase(input.begin() + i);
				i--;
			}
			else break;
	}
	if (input.length() == 0) return;
	if (input[input.length() - 1] == ' '){
		for (unsigned i = input.length() - 1; i >= 0; i--){
			if (input[i] != ' '){
				input.erase(input.begin() + i + 1, input.end());
				break;
			}
		}
	}
	for (unsigned i = 1; i < input.length(); i++){
		if (input[i] == ' ' && input[i - 1] == ' '){
			input.erase(input.begin() + i);
			i--;
		}
	}
	if (input.length() > 1 && input[input.length() - 1] == ';' && input[input.length() - 2] == ' '){
		input.erase(input.end() - 1);
	}
	if (version){
		if (input[0] == '#'){
			for (unsigned j = 1; j < input.length(); j++)
				if (input[j] == '3' && input[j - 1] == '3'){
					input[j - 1] = '1';
					version = false;
					break;
				}
		}
	}
	if (input[0] == 'l' && input[1] == 'a' && input[2] == 'y'){
		for (unsigned i = 0; i < input.length(); i++)
			if (input[i] == '='){
				location.push_back(atoi(input.c_str() + i + 1));
				for (unsigned j = i + 1; j < input.length(); j++){
					if (input[j] == ')'){
						input.erase(input.begin(), input.begin() + j + 2);
						break;
					}
				}
				break;
			}
		for (unsigned i = input.length() - 1; i >= 0; i--){
			if (input[i] == ' '){
				string tmp = "";
				for (unsigned j = i + 1; j < input.length() - 1; j++)
					tmp += input[j];
				name.push_back(tmp);
				break;
			}
		}
	}
}

GLuint LoadShaders(const char* vertexShader, const char* fragmentShader)
{
	GLint versionMajor, versionMinor;
	GLboolean compatibility = false;
	vector <int> location;
	vector <string> name;
    GLuint vsID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fsID = glCreateShader(GL_FRAGMENT_SHADER);
	glGetIntegerv(GL_MAJOR_VERSION, &versionMajor);
	glGetIntegerv(GL_MINOR_VERSION, &versionMinor);

	if (versionMajor == 3 && versionMinor < 3){
		cout << "Your computer only supports OpenGL " << versionMajor << "." << versionMajor << ". The shader files will be modified (Read the comment)" << endl;
		compatibility = true;
	}
	if(versionMajor < 3){
		cout << "Your computer needs to support OpenGL 3.0+ to run this program" << endl;
		cout << "Please use a computer that has at least Intel HD graphic card" << endl;
		return -1;
	}
    //Read vertex shader from file
    string vsCode;
    ifstream vsStream(vertexShader);
    if(vsStream.is_open())
    {
        while(!vsStream.eof())
        {
            string temp;
            getline(vsStream, temp);
            if(compatibility)
                format(temp, location, name);
            vsCode += temp + "\n";
        }
        vsStream.close();
    }
    else
    {
        cout << "Can't open " << vertexShader << endl;
        return 0;
    }

    //Read fragment shader from file
    string fsCode;
    ifstream fsStream(fragmentShader);
    if(fsStream.is_open())
    {
        while(!fsStream.eof())
        {
            string temp;
            getline(fsStream, temp);
            if(compatibility)
                format(temp, location, name);
            fsCode += temp + "\n";
        }
        fsStream.close();
    }
    else
    {
        cout << "Can't open " << fragmentShader << endl;
        return 0;
    }

    GLint status = GL_FALSE;
    int infoLogLength1;
	int infoLogLength2;
	int infoLogLength3;

    //Compile vertex shader
    char const *vsPointer = vsCode.c_str();
    glShaderSource(vsID, 1, &vsPointer, NULL);
    glCompileShader(vsID);

    //Check vertex shader
    glGetShaderiv(vsID, GL_COMPILE_STATUS, &status);
	glGetShaderiv(vsID, GL_INFO_LOG_LENGTH, &infoLogLength1);
	GLchar *vsInfoLog = new char[infoLogLength1+1];
    glGetShaderInfoLog(vsID, infoLogLength1, NULL, vsInfoLog);
	cout << "Compiling vertex shader:" << endl << vsInfoLog << endl;

    //Compile fragment shader
    char const *fsPointer = fsCode.c_str();
    glShaderSource(fsID, 1, &fsPointer, NULL);
    glCompileShader(fsID);

    //Check fragment shader
	glGetShaderiv(fsID, GL_COMPILE_STATUS, &status);
	glGetShaderiv(fsID, GL_INFO_LOG_LENGTH, &infoLogLength2);
	GLchar *fsInfoLog = new char[infoLogLength2+1];
	glGetShaderInfoLog(fsID, infoLogLength2, NULL, fsInfoLog);
	cout << "Compiling fragment shader:" << endl << fsInfoLog << endl;

    //Link the program
    GLuint theProgram = glCreateProgram();
    glAttachShader(theProgram, vsID);
    glAttachShader(theProgram, fsID);

	for (unsigned i = 0; i < location.size(); i++){
		glBindAttribLocation(theProgram, location[i], name[i].c_str());
	}

    glLinkProgram(theProgram);

    //Check the program
    glGetProgramiv(theProgram, GL_LINK_STATUS, &status);
	glGetProgramiv(theProgram, GL_INFO_LOG_LENGTH, &infoLogLength3);
	GLchar *pInfoLog = new char[infoLogLength3 + 1];
	glGetProgramInfoLog(theProgram, infoLogLength3, NULL, pInfoLog);
	cout << "Linking program:" << endl << pInfoLog << endl;
    glDeleteShader(vsID);
    glDeleteShader(fsID);
    return theProgram;
}
