#version 330

in vec2 TexCoord;
out vec4 outColor;

uniform vec4 coeff;
uniform sampler2D sampler1;
uniform sampler2D sampler2;
uniform sampler2D sampler3;
uniform sampler2D sampler4;

void main()
{
	outColor = texture(sampler1, TexCoord)*coeff.x + texture(sampler2, TexCoord)*coeff.y + texture(sampler3, TexCoord)*coeff.z + texture(sampler4, TexCoord)*coeff.w;
}