#version 330

in vec2 TexCoor1;
in vec2 TexCoor2;
in vec2 TexCoor3;
in vec2 TexCoor4;

out vec4 outColor;

uniform sampler2D sampler;

void main(){
	outColor = (texture(sampler, TexCoor1) + texture(sampler, TexCoor2) + texture(sampler, TexCoor3) + texture(sampler, TexCoor4)) * 0.25f;
}
