#version 330

layout(location = 0) in vec3 PosAttribute;

uniform mat4 viewMatrix;
uniform mat4 projectMatrix;

out vec3 TexCoord;
void main(){
	TexCoord = PosAttribute;
	gl_Position =  projectMatrix * viewMatrix * vec4(PosAttribute, 1.0);
}