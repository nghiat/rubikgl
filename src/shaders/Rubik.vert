#version 330

layout(location = 0) in vec3 vert;
layout(location = 1) in vec3 vertNormal;
layout(location = 2) in vec4 vertColor;
layout(location = 3) in vec3 PosAttribute;

out vec3 fragVert;
out vec3 fragNormal;
out vec4 fragColor;
out vec3 reflectVector;
out vec3 refractVector;
out vec3 TexCoord;

uniform mat4 MVP;
uniform mat4 viewMatrix;
uniform int mode;
uniform float eta;
uniform vec3 eye;

void main(){
	fragVert = vert;
	fragNormal = normalize(vertNormal);
	fragColor = vertColor;
	TexCoord = mat3(viewMatrix) * PosAttribute;
	reflectVector = reflect(normalize(vert - eye), fragNormal);
	refractVector = refract(normalize(vert - eye), fragNormal, eta);

	gl_Position = MVP * vec4(vert, 1.0f);
}