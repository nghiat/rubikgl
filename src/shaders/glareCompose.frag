#version 330

in vec2 TexCoord;
out vec4 outColor;

uniform vec4 mixCoeff;
uniform sampler2D sampler1;
void main()
{
	outColor = texture(sampler1, TexCoord)*mixCoeff.x;
}