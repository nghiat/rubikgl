#version 330

in vec2 TexCoor;
out vec4 outColor;

uniform float threshold;
uniform float scalar;
uniform sampler2D sampler;

void main(){
	outColor = max((texture(sampler, TexCoor) - threshold) * scalar, vec4(0.0f, 0.0f, 0.0f, 0.0f));
}