#version 330

in vec2 TexCoord;
uniform sampler2D sampler;

out vec4 outColor;

void main(){
	outColor = texture(sampler, TexCoord);
}