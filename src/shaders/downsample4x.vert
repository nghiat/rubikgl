#version 330

layout(location = 0) in vec3 Pos;
layout(location = 1) in vec2 Tex;

uniform vec2 twoTexelSize;

out vec2 TexCoor1;
out vec2 TexCoor2;
out vec2 TexCoor3;
out vec2 TexCoor4;

void main(){
	TexCoor1 = Tex;
	TexCoor2 = Tex + vec2(twoTexelSize.x, 0.0f);
	TexCoor3 = Tex + vec2(twoTexelSize.x, twoTexelSize.y);
	TexCoor4 = Tex + vec2(0.0f, twoTexelSize.y);
	gl_Position = vec4(Pos, 1.0f);
}