#version 330

uniform struct Light{
	vec3 position;
	vec3 intensities;	//a.k.a the color of the light
} light;

uniform mat4 tiModel;
uniform bool evMapping;
uniform samplerCube skybox;
uniform int mode;
uniform vec3 eye;
uniform vec4 Phong;
uniform float factor;

in vec3 TexCoord;
in vec3 fragVert;
in vec3 fragNormal;
in vec4 fragColor;
in vec3 reflectVector;
in vec3 refractVector;
out vec4 outColor;

float my_fresnel(vec3 I, vec3 N, float power,  float scale,  float bias)
{
    return max(0.0f, min(1, bias + scale * pow(1.0f + dot(I, N), power)));
}

void main(){
	vec3 reflectColor = texture(skybox, reflectVector).rgb;
	vec3 refractColor = texture(skybox, refractVector).rgb;
	vec3 tranformedVector = fragNormal;
	vec3 surfaceToLight = normalize(fragVert - light.position);
	vec3 surfaceToEye = normalize(fragVert - eye);
	float specular = pow(max(0.0, -dot(reflect(surfaceToLight, tranformedVector), surfaceToEye)), Phong.w) * Phong.z;
	float diffuse = max(0.0,dot(surfaceToLight, tranformedVector)) * Phong.y;
	float ambient = Phong.x;
	if(!evMapping || mode == 0){
		outColor = vec4(fragColor.rgb * (diffuse + ambient) + specular * light.intensities, fragColor.a);
	}
	else{
		switch (mode){
			case 1: outColor = vec4(mix(fragColor.rgb, reflectColor, factor), 1.0f); break;
			case 2: outColor = vec4(mix(fragColor.rgb, refractColor, factor), 1.0f); break;
			case 3: outColor = vec4(mix(refractColor, reflectColor, my_fresnel(surfaceToLight, tranformedVector, 5.0f, 1.0f, 0.1f)), 1.0f); break;
		}
	}
}