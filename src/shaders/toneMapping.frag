#version 330

in vec2 TexCoord;
out vec4 outColor;

uniform sampler2D sceneTex;
uniform sampler2D blurTex;
uniform float blurAmount;
uniform float exposure;
uniform float gamma;

float A = 0.15f;
float B = 0.50f;
float C = 0.10f;
float D = 0.20f;
float E = 0.02f;
float F = 0.30f;
float W = 11.2f;

vec3 filmicTonemapping(vec3 x)
{
  return ((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E/F;
}

float vignette(vec2 pos, float inner, float outer)
{
  float r = length(pos);
  r = 1.0f - smoothstep(inner, outer, r);
  return r;
}

void main()
{
  vec4 scene = texture(sceneTex, TexCoord);
  vec4 blurred = texture(blurTex, TexCoord);
  vec3 c = mix(scene.rgb, blurred.rgb, blurAmount);
  c = c * exposure / 0.5f;
  c = c * vignette(TexCoord*2.0f - 1.0f, 0.55f, 1.5f);
  float ExposureBias = 1.0f;
  c = filmicTonemapping(ExposureBias * c);
  vec3 whiteScale = 1.0 / filmicTonemapping(vec3(W, W, W));
  c = c*whiteScale;
  c.r = pow(c.r, gamma);
  c.g = pow(c.g, gamma);
  c.b = pow(c.b, gamma);
  outColor = vec4(c, 1.0);
}