#version 330

in vec2 TexCoor;
out vec4 outColor;

uniform sampler2D sampler;
uniform float resolution;
uniform float radius;
uniform vec2 dir;
uniform float s;

float gaussian(float x, float sd){
	return exp(-(x*sd) * (x*sd));
}

void main(){
	vec4 sum = vec4(0.0f);
	float blur = 1 / resolution;
	float hstep = dir.x;
	float vstep = dir.y;
	float gauSum = 0.0f;
	for(int i = 0; i < 2 * radius + 1; i++){
		gauSum += gaussian(i - radius, s);
	}
    outColor = vec4(sum.rgb, 1.0);
    sum+= texture(sampler, TexCoor) * gaussian(0, s) / gauSum;
    for(int i = 1; i <= radius; i++){
    	sum += texture(sampler, vec2(TexCoor.x - i*blur*hstep, TexCoor.y - i*blur*vstep)) * gaussian(i, s) / gauSum;
    	sum += texture(sampler, vec2(TexCoor.x + i*blur*hstep, TexCoor.y + i*blur*vstep)) * gaussian(i, s) / gauSum;
    }
    outColor = vec4(sum.rgb, 1.0f);
}