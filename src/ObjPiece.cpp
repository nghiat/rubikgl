#define TINYOBJLOADER_IMPLEMENTATION
#include "ObjPiece.h"
#include <string>
#include <iostream>
#include <stdlib.h>

ObjPiece::ObjPiece(){
}

ObjPiece::ObjPiece(const char* filename)
{
	std::string err;
	bool ret = tinyobj::LoadObj(shapes, materials, err, filename);
	color = glm::vec4(0.157f, 0.157f, 0.157f, 1.0f);
	if (!err.empty()){
		std::cerr << err << std::endl;
	}
}

ObjPiece::ObjPiece(const ObjPiece& clone){
	this->materials = clone.materials;
	this->shapes = clone.shapes;
	this->color = clone.color;
}

ObjPiece& ObjPiece::operator=(const ObjPiece& clone){
	this->materials = clone.materials;
	this->shapes = clone.shapes;
	this->color = clone.color;
	return *this;
}

ObjPiece::~ObjPiece()
{
}
