#pragma once

#include <GL/gl3w.h>

//Convert cross image to 6 separate images
GLuint convertCrossToCubemap(const char* filename);