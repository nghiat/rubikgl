#pragma once

#include <GL/gl3w.h>
//Create class to group framebuffer object, texture object and render buffer object
class RenderTexture
{
public:
	RenderTexture();
	~RenderTexture();
	void Init(int width, int height, int depth);
	void resizeFB(int w, int h);
	void bindFB();
	void bind();
	void unBind();
	int getWidth();
	int getHeight();
private:
	int tWidth;
	int tHeight;
	GLuint fbo;
	GLuint fto;
	GLuint fdo;
};

