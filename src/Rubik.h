#pragma once

#define GLEW_STATIC
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <AntTweakBar.h>
#include "pieces.h"
#include "RenderTexture.h"

//Swipe duration
#define DURATION 0.5f
#define LEVEL 4

struct Light{
	glm::vec3 position;
	glm::vec3 intensities;
} light;

//Program IDs
GLuint rubikProgram, skyboxProgram, downsampleProgram, downsample4xProgram, extractHLProgram, blurProgram, gaussianComposeProgram, glareComposeProgram, toneMappingProgram;
//Pointers of objects
GLuint* vbo[TOTALPIECES];
GLuint* nbo[TOTALPIECES];
GLuint* ibo[TOTALPIECES];
GLuint* cbo[TOTALPIECES];
GLuint skyboxVertices;
GLuint skyboxIndices;
//Texture IDs
GLuint	hdr_tex;
//Vertex array object
GLuint vao;
//uniform variables ID
GLuint quadBO, texBO;
GLuint MVPID, viewID, tiModel;
GLuint evMappingID, modeID, eyeID, etaID, PhongID, factorID;
GLuint viewSkybox, projectionSkybox;
GLuint positionID, intensitiesID;
GLuint twoTexelSizeID;
GLuint thresholdID, scalarID;
GLuint resolutionID, radiusID, dirID, sID;
GLuint coeffID;
GLuint mixCoeffID;
GLuint blurAmountID, exposureID, gammaID;

RenderTexture *mainRT, *finalRT;
RenderTexture *compose_buffer[LEVEL], *blur_buffer[LEVEL], *temp_buffer[LEVEL];

//width, height of the window
int w, h;
//for mouse click callback
bool mouseClicked = false;
//Mouse position
double mouseX, mouseY;

//Vars for transformations
glm::vec3 eye, centerLook, up, rotationAxis1, rotationAxis2;
glm::mat4 modelMatrix, viewMatrix, perspectiveMatrix, MVPMatrix, transposeInversedMatrix;

//All for swiping
int lastPiece = -1;
int swipePlane;
float direction;
bool swiping = false;
double startAnimation;
double lastTime;
float totalAngle;
bool third_person_mode = false;

//3.0f / bloom width
float s[4] = { 0.42f, 0.25f, 0.15f, 0.10f };

//Tweakbar bars
bool evMapping = false;
int renderMode = 0;
float eta = 1.0f;
float factor = 0.5f;
float exposure = 1.4f;
bool HDR = false;
//Phong components
//ambient, diffuse, specular, shininess
glm::vec4 Phong[2];

//Reorder after swiping
void reorder(int plane); 
//Set up everything (vertices, indices, normals)
void init();
//Display function
void display();
//find a piece corresponding to the coordinates
//Also find its face
glm::ivec2 findPiece(glm::vec3 coor);

//For scrumble function
bool scrumble = false;
int times = 0;

TwBar *bar;

//load all the textures
void initTexture();

//Initialize vectors, matricies, GLSL programs, uniform IDs
void initVars();

//Initialize vertex buffer objects, index buffer objects, normal buffer objects, color buffer objects, framebuffer objects with their textures, renderbuffers
void initObjects();

//Initialize all
void init();

//Draw quad
void DrawAxisAlignedQuad();

//Swap pieces positions after swiping
void reorder();

//Update pieces position while swiping
void updateRubik();

//Draw the cube texture
void drawSkybox();

//Draw the rubik
void drawRubik();

//Downsample 2x in each dimension
void downsample(RenderTexture* src, RenderTexture* des);

//Downsample 4x in each dimension
void downsample4x(RenderTexture* src, RenderTexture* des);

//Extract highlight area
void extractHL(RenderTexture* src, RenderTexture* des);

//Apply vertical and horizontal gaussian blur
void blur(RenderTexture* src, RenderTexture* des, RenderTexture* temp, float radius, float s);

//Compose all the blur framebuffers
void ComposeEffect();

//Map the original scene with the post-processed scene
void toneMappingPass();

//main display function
void display();

//Find pieces based on its world coordinates
glm::ivec2 findPiece(glm::vec3 coor);


void swipeCallback(GLFWwindow *window, double xpos, double ypos);
void mousebuttonCallback(GLFWwindow* window, int button, int action, int mods);
void thirdPersonCallback(GLFWwindow *window, double xpos, double ypos);
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void framebufferCallback(GLFWwindow *window, int width, int height);