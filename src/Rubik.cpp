#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include "LoadShaders.h"
#include "Rubik.h"
#include "CubeData.h"
#include "HDRImage.h"

using namespace std;
using namespace glm;

void initTexture(){
	hdr_tex = convertCrossToCubemap("assets/altar_cross_mmp_s.hdr");

	glGenBuffers(1, &skyboxVertices);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVertices);
	glBufferData(GL_ARRAY_BUFFER, 4 * 8 * 6 * 4, verticesCube, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &skyboxIndices);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skyboxIndices);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * 6 * 2, indicesCube, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void initVars()
{
	rotationAxis1 = vec3(0.0f, 1.0f, 0.0f);
	modelMatrix = mat4(1.0f);
	eye = vec3(7.0f);
	centerLook = vec3(0.0f);
	up = vec3(0.0f, 1.0f, 0.0f);
	viewMatrix = lookAt(eye, centerLook, up);
	rotationAxis2 = cross(centerLook - eye, rotationAxis1);
	perspectiveMatrix = perspective(45.0f, float(w * 1.0f / h), 0.1f, 100.0f);
	MVPMatrix = perspectiveMatrix * viewMatrix * modelMatrix;
	Phong[0] = vec4(0.5, 0.6, 0.4, 16.0); //shiny plastic
	Phong[1] = vec4(0.8, 0.2, 0.1, 8.0); //shiny paper

	//Use to transform normals in fragment shader
	transposeInversedMatrix = transpose(inverse(modelMatrix));
	//Set up the light
	light.position = vec3(10.0f, 10.0f, 10.0f);
	light.intensities = vec3(1.0f, 1.0f, 1.0f);

	rubikProgram = LoadShaders("shaders/Rubik.vert", "shaders/Rubik.frag");
	skyboxProgram = LoadShaders("shaders/skybox.vert", "shaders/skybox.frag");
	//Simply do nothing
	downsampleProgram = LoadShaders("shaders/downsample.vert", "shaders/downsample.frag");
	downsample4xProgram = LoadShaders("shaders/downsample4x.vert", "shaders/downsample4x.frag");
	extractHLProgram = LoadShaders("shaders/extractHL.vert", "shaders/extractHL.frag");
	blurProgram = LoadShaders("shaders/blur.vert", "shaders/blur.frag");
	gaussianComposeProgram = LoadShaders("shaders/gaussianCompose.vert", "shaders/gaussianCompose.frag");
	glareComposeProgram = LoadShaders("shaders/glareCompose.vert", "shaders/glareCompose.frag");
	toneMappingProgram = LoadShaders("shaders/toneMapping.vert", "shaders/toneMapping.frag");


	MVPID = glGetUniformLocation(rubikProgram, "MVP");
	tiModel = glGetUniformLocation(rubikProgram, "tiModel");
	positionID = glGetUniformLocation(rubikProgram, "light.position");
	intensitiesID = glGetUniformLocation(rubikProgram, "light.intensities");
	evMappingID = glGetUniformLocation(rubikProgram, "evMapping");
	modeID = glGetUniformLocation(rubikProgram, "mode");
	eyeID = glGetUniformLocation(rubikProgram, "eye");
	etaID = glGetUniformLocation(rubikProgram, "eta");
	PhongID = glGetUniformLocation(rubikProgram, "Phong");
	viewID = glGetUniformLocation(rubikProgram, "viewMatrix");
	factorID = glGetUniformLocation(rubikProgram, "factor");

	viewSkybox = glGetUniformLocation(skyboxProgram, "viewMatrix");
	projectionSkybox = glGetUniformLocation(skyboxProgram, "projectMatrix");

	twoTexelSizeID = glGetUniformLocation(downsample4xProgram, "twoTexelSize");

	thresholdID = glGetUniformLocation(extractHLProgram, "threshold");
	scalarID = glGetUniformLocation(extractHLProgram, "scalar");

	resolutionID = glGetUniformLocation(blurProgram, "resolution");
	radiusID = glGetUniformLocation(blurProgram, "radius");
	dirID = glGetUniformLocation(blurProgram, "dir");
	sID = glGetUniformLocation(blurProgram, "s");

	coeffID = glGetUniformLocation(gaussianComposeProgram, "coeff");

	mixCoeffID = glGetUniformLocation(glareComposeProgram, "mixCoeff");

	blurAmountID = glGetUniformLocation(toneMappingProgram, "blurAmount");
	exposureID = glGetUniformLocation(toneMappingProgram, "exposure");
	gammaID = glGetUniformLocation(toneMappingProgram, "gamma");

}

void initObjects()
{
	//Loading all pieces
	loadPieces();

	float quad[] = {
		-1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f
	};
	float texCoor[] = {
		0, 0, 1, 0, 0, 1, 1, 1
	};

	glGenBuffers(1, &quadBO);
	glBindBuffer(GL_ARRAY_BUFFER, quadBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);

	glGenBuffers(1, &texBO);
	glBindBuffer(GL_ARRAY_BUFFER, texBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoor), texCoor, GL_STATIC_DRAW);

	//Set up all buffers
	for (int i = 0; i < TOTALPIECES; i++){
		int shapeSize = pieces[i].piece.shapes.size() + pieces[i].stickers.size();//Technically, these two are not the same. But shapes.size() = 1 so sticker.size() = sticker shape size
		vbo[i] = new GLuint[shapeSize];
		nbo[i] = new GLuint[shapeSize];
		ibo[i] = new GLuint[shapeSize];
		cbo[i] = new GLuint[shapeSize];

		glGenBuffers(shapeSize, vbo[i]);
		glGenBuffers(shapeSize, nbo[i]);
		glGenBuffers(shapeSize, ibo[i]);
		glGenBuffers(shapeSize, cbo[i]);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[i][0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[i].piece.shapes[0].mesh.positions.size(), &pieces[i].piece.shapes[0].mesh.positions[0], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, nbo[i][0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[i].piece.shapes[0].mesh.normals.size(), &pieces[i].piece.shapes[0].mesh.normals[0], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[i][0]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(float) * pieces[i].piece.shapes[0].mesh.indices.size(), &pieces[i].piece.shapes[0].mesh.indices[0], GL_STATIC_DRAW);
		vector<float> colorBuffer;
		for (unsigned k = 0; k < pieces[i].piece.shapes[0].mesh.indices.size(); k++){
			colorBuffer.push_back(pieces[i].piece.color[0]);
			colorBuffer.push_back(pieces[i].piece.color[1]);
			colorBuffer.push_back(pieces[i].piece.color[2]);
			colorBuffer.push_back(pieces[i].piece.color[3]);
		}
		glBindBuffer(GL_ARRAY_BUFFER, cbo[i][0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * colorBuffer.size(), &colorBuffer[0], GL_STATIC_DRAW);
		for (unsigned j = 0; j < pieces[i].stickers.size(); j++){
			glBindBuffer(GL_ARRAY_BUFFER, vbo[i][j + 1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[i].stickers[j].shapes[0].mesh.positions.size(), &pieces[i].stickers[j].shapes[0].mesh.positions[0], GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, nbo[i][j + 1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[i].stickers[j].shapes[0].mesh.normals.size(), &pieces[i].stickers[j].shapes[0].mesh.normals[0], GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[i][j + 1]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(float) * pieces[i].stickers[j].shapes[0].mesh.indices.size(), &pieces[i].stickers[j].shapes[0].mesh.indices[0], GL_STATIC_DRAW);
			vector<float> stickerColor;
			for (unsigned k = 0; k < pieces[i].stickers[j].shapes[0].mesh.indices.size(); k++){
				stickerColor.push_back(pieces[i].stickers[j].color[0]);
				stickerColor.push_back(pieces[i].stickers[j].color[1]);
				stickerColor.push_back(pieces[i].stickers[j].color[2]);
				stickerColor.push_back(pieces[i].stickers[j].color[3]);
			}
			glBindBuffer(GL_ARRAY_BUFFER, cbo[i][j + 1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * stickerColor.size(), &stickerColor[0], GL_STATIC_DRAW);
		}
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	int postProcessingWidth = 1024;
	int postProcessingHeight = 1024;
	mainRT = new RenderTexture();
	mainRT->Init(w, h, 24);

	finalRT = new RenderTexture();
	finalRT->Init(postProcessingHeight / 2, postProcessingHeight / 2, 0);
	postProcessingWidth = 256;
	postProcessingHeight = 256;
	for (int i = 0; i < LEVEL; i++){
		compose_buffer[i] = new RenderTexture();
		compose_buffer[i]->Init(postProcessingWidth, postProcessingHeight, 0);
		blur_buffer[i] = new RenderTexture();
		blur_buffer[i]->Init(postProcessingWidth, postProcessingHeight, 0);
		temp_buffer[i] = new RenderTexture();
		temp_buffer[i]->Init(postProcessingWidth, postProcessingHeight, 0);
		postProcessingHeight /= 2;
		postProcessingWidth /= 2;
	}
}

void init(){
	initTexture();
	initVars();
	initObjects();
}

void DrawAxisAlignedQuad()
{
	glDisable(GL_DEPTH_TEST);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, quadBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, texBO);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void reorder(){
	FullPiece temp[9];
	for (int i = 0; i < 8; i++){
		temp[i] = pieces[swipe[swipePlane][i]];
	}
	for (int i = 0; i < 8; i++){
		//if direction < 0, we need to add 8 to i so i won't be negative
		pieces[swipe[swipePlane][(i + 8 + 2 * int(direction)) % 8]] = temp[i];
	}
	for (int i = 0; i < 9; i++){
		int index = swipe[swipePlane][i];
		glBindBuffer(GL_ARRAY_BUFFER, vbo[index][0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[index].piece.shapes[0].mesh.positions.size(), &pieces[index].piece.shapes[0].mesh.positions[0], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, nbo[index][0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[index].piece.shapes[0].mesh.normals.size(), &pieces[index].piece.shapes[0].mesh.normals[0], GL_DYNAMIC_DRAW);
		for (unsigned j = 0; j < pieces[index].stickers.size(); j++){
			glBindBuffer(GL_ARRAY_BUFFER, vbo[index][j + 1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[index].stickers[j].shapes[0].mesh.positions.size(), &pieces[index].stickers[j].shapes[0].mesh.positions[0], GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, nbo[index][j + 1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[index].stickers[j].shapes[0].mesh.normals.size(), &pieces[index].stickers[j].shapes[0].mesh.normals[0], GL_DYNAMIC_DRAW);
			vector<float> stickerColor;
			for (unsigned k = 0; k < pieces[index].stickers[j].shapes[0].mesh.indices.size(); k++){
				stickerColor.push_back(pieces[index].stickers[j].color[0]);
				stickerColor.push_back(pieces[index].stickers[j].color[1]);
				stickerColor.push_back(pieces[index].stickers[j].color[2]);
				stickerColor.push_back(pieces[index].stickers[j].color[3]);
			}
			glBindBuffer(GL_ARRAY_BUFFER, cbo[index][j + 1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * stickerColor.size(), &stickerColor[0], GL_STATIC_DRAW);
		}
	}
}

void updateRubik()
{
	double delta = glfwGetTime() - startAnimation;
	double currentTime = glfwGetTime();
	float angle = float((currentTime - lastTime) * PI / 2 / DURATION);
	//If it is the last rotation, we round it up to PI / 2
	if (delta >= DURATION){
		angle = PI / 2.0f - totalAngle;
	}
	totalAngle += angle;
	for (int i = 0; i < 9; i++){
		int index = swipe[swipePlane][i];
		pieces[index].piece = transform(pieces[index].piece, vec3(0.0f), angle, axis[swipePlane] * direction);
		for (unsigned j = 0; j < pieces[index].stickers.size(); j++){
			pieces[index].stickers[j] = transform(pieces[index].stickers[j], vec3(0.0f), angle, axis[swipePlane] * direction);
		}
		glBindBuffer(GL_ARRAY_BUFFER, vbo[index][0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[index].piece.shapes[0].mesh.positions.size(), &pieces[index].piece.shapes[0].mesh.positions[0], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, nbo[index][0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[index].piece.shapes[0].mesh.normals.size(), &pieces[index].piece.shapes[0].mesh.normals[0], GL_DYNAMIC_DRAW);
		for (unsigned j = 0; j < pieces[index].stickers.size(); j++){
			glBindBuffer(GL_ARRAY_BUFFER, vbo[index][j + 1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[index].stickers[j].shapes[0].mesh.positions.size(), &pieces[index].stickers[j].shapes[0].mesh.positions[0], GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, nbo[index][j + 1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pieces[index].stickers[j].shapes[0].mesh.normals.size(), &pieces[index].stickers[j].shapes[0].mesh.normals[0], GL_DYNAMIC_DRAW);
		}
	}

	lastTime = currentTime;
	if (delta >= DURATION){
		swiping = false;
		lastPiece = -1;
		reorder();
	}
}

void drawSkybox()
{
	glUseProgram(skyboxProgram);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, hdr_tex);
	glUniformMatrix4fv(viewSkybox, 1, GL_FALSE, &viewMatrix[0][0]);
	glUniformMatrix4fv(projectionSkybox, 1, GL_FALSE, &perspectiveMatrix[0][0]);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVertices);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 32, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skyboxIndices);
	glDrawElements(GL_TRIANGLES, 6 * 6, GL_UNSIGNED_SHORT, 0);

	glDisableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glUseProgram(0);
}

void drawRubik()
{
	glEnable(GL_DEPTH_TEST);
	glUseProgram(rubikProgram);

	glUniformMatrix4fv(MVPID, 1, GL_FALSE, &MVPMatrix[0][0]);
	glUniformMatrix4fv(tiModel, 1, GL_FALSE, &transposeInversedMatrix[0][0]);
	glUniformMatrix4fv(viewID, 1, GL_FALSE, &viewMatrix[0][0]);
	glUniform3fv(positionID, 1, &light.position[0]);
	glUniform3fv(intensitiesID, 1, &light.intensities[0]);
	glUniform1i(evMappingID, evMapping);
	glUniform1i(modeID, renderMode);
	glUniform1f(factorID, factor);
	glUniform1f(etaID, eta);
	glUniform3f(eyeID, eye[0], eye[1], eye[2]);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, hdr_tex);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	glBindBuffer(GL_ARRAY_BUFFER, skyboxVertices);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 32, 0);

	for (int i = 0; i < TOTALPIECES; i++){
		glUniform4fv(PhongID, 1, &Phong[0][0]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[i][0]);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, nbo[i][0]);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, cbo[i][0]);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[i][0]);
		glDrawElements(GL_TRIANGLES, pieces[i].piece.shapes[0].mesh.indices.size(), GL_UNSIGNED_INT, 0);
		glUniform4fv(PhongID, 1, &Phong[1][0]);
		for (unsigned j = 0; j < pieces[i].stickers.size(); j++){
			glBindBuffer(GL_ARRAY_BUFFER, vbo[i][j + 1]);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, nbo[i][j + 1]);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, cbo[i][j + 1]);
			glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 0, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[i][j + 1]);
			glDrawElements(GL_TRIANGLES, pieces[i].stickers[j].shapes[0].mesh.indices.size(), GL_UNSIGNED_INT, 0);
		}
	}

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glUseProgram(0);
}

void downsample(RenderTexture* src, RenderTexture* des){
	des->bindFB();

	glUseProgram(downsampleProgram);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	glActiveTexture(GL_TEXTURE0);
	src->bind();
	DrawAxisAlignedQuad();
	src->unBind();
}

void downsample4x(RenderTexture* src, RenderTexture* des){
	des->bindFB();

	glUseProgram(downsample4xProgram);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	float twoPixelX = float(2.0) / w;
	float twoPixelY = float(2.0) / h;

	glUniform2f(twoTexelSizeID, twoPixelX, twoPixelY);

	glActiveTexture(GL_TEXTURE0);
	src->bind();
	DrawAxisAlignedQuad();
	src->unBind();
}

void extractHL(RenderTexture* src, RenderTexture* des){
	des->bindFB();

	glUseProgram(extractHLProgram);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	glActiveTexture(GL_TEXTURE0); 
	src->bind();
	glUniform1f(thresholdID, 1.0f);
	glUniform1f(scalarID, 0.3f);
	DrawAxisAlignedQuad();
	src->unBind();
}

void blur(RenderTexture* src, RenderTexture* des, RenderTexture* temp, float radius, float s){
	temp->bindFB();
	glUseProgram(blurProgram);

	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);
	//horizontal blur
	glUniform1f(resolutionID, float(src->getWidth()));
	glUniform1f(radiusID, radius);
	glUniform2f(dirID, 1.0f, 0.0f);
	glUniform1f(sID, s);
	glActiveTexture(GL_TEXTURE0);
	src->bind();
	DrawAxisAlignedQuad();
	src->unBind();
	glUseProgram(0);

	//vertical blur
	des->bindFB();
	glUseProgram(blurProgram);
	glUniform1f(resolutionID, float(src->getHeight()));
	glUniform1f(radiusID, radius);
	glUniform2f(dirID, 0.0f, 1.0f);
	glUniform1f(sID, s);
	glActiveTexture(GL_TEXTURE0);
	temp->bind();
	DrawAxisAlignedQuad();
	temp->unBind();
}

void ComposeEffect(){
	////////compose gaussian blur buffers///////////////
	compose_buffer[0]->bindFB();
	glUseProgram(gaussianComposeProgram);
	glUniform1i(glGetUniformLocation(gaussianComposeProgram, "sampler1"), 0);
	glUniform1i(glGetUniformLocation(gaussianComposeProgram, "sampler2"), 1);
	glUniform1i(glGetUniformLocation(gaussianComposeProgram, "sampler3"), 2);
	glUniform1i(glGetUniformLocation(gaussianComposeProgram, "sampler4"), 3);
	glActiveTexture(GL_TEXTURE0);
	blur_buffer[0]->bind();
	glActiveTexture(GL_TEXTURE1);
	blur_buffer[1]->bind();
	glActiveTexture(GL_TEXTURE2);
	blur_buffer[2]->bind();
	glActiveTexture(GL_TEXTURE3);
	blur_buffer[3]->bind();

	float coeff[4] = { 0.3f, 0.3f, 0.25f, 0.20f };

	glUniform4fv(coeffID, 1, coeff);
	DrawAxisAlignedQuad();

	////////////////final glare composition/////////////
	finalRT->bindFB();
	glUseProgram(glareComposeProgram);

	glActiveTexture(GL_TEXTURE0);
	compose_buffer[0]->bind();

	float mixCoeff[4] = { 1.2f, 0.8f, 0.1f, 0.0f};

	glUniform4fv(mixCoeffID, 1, mixCoeff);
	DrawAxisAlignedQuad();
}

void toneMappingPass(){
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, w, h);

	glUseProgram(toneMappingProgram);
	glUniform1i(glGetUniformLocation(toneMappingProgram, "sceneTex"), 0);
	glUniform1i(glGetUniformLocation(toneMappingProgram, "blurTex"), 1);
	glActiveTexture(GL_TEXTURE0);
	mainRT->bind();
	glActiveTexture(GL_TEXTURE1);
	finalRT->bind();

	//adaptive exposure adjustment in log space

	float newExp = (float)(10.0f * log(exposure + 0.0001));

	glUniform1f(blurAmountID, 0.33f);
	glUniform1f(exposureID, newExp);
	glUniform1f(gammaID, 1.0f / 1.8f);

	DrawAxisAlignedQuad();
}

void display(){
	if (scrumble && !swiping){
		if (times < 9){
			startAnimation = lastTime = glfwGetTime();
			swiping = true;
			totalAngle = 0.0;
			times++;
			swipePlane = rand() % 9;
			direction = float(pow(-1, rand() % 2));
		}
		else{
			times = 0;
			scrumble = false;
		}
	}
	if (swiping){
		updateRubik();
	}
	
	if (evMapping)
	{
		if (HDR){
			mainRT->bindFB();
		}
		else{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, w, h);
		drawSkybox();
		drawRubik();

		if (HDR){
			downsample4x(mainRT, blur_buffer[0]);
			extractHL(blur_buffer[0], compose_buffer[0]);
			blur(compose_buffer[0], blur_buffer[0], temp_buffer[0], float(int(3.0f / s[0])), s[0]);

			for (int i = 1; i < LEVEL; i++) {
				downsample(compose_buffer[i - 1], compose_buffer[i]);
				blur(compose_buffer[i], blur_buffer[i], temp_buffer[i], float(int(3.0f / s[i])), s[i]);
			}

			ComposeEffect();

			toneMappingPass();
		}
	}
	else{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		drawRubik();
	}
}

ivec2 findPiece(vec3 coor){
	int result = 0;
	int face = 0;
	int temp;
	if (!(coor[0] > -2.8f && coor[0] < 2.8f && coor[1] > -2.8f && coor[1] < 2.8f && coor[2] > -2.8f && coor[2] < 2.8f))
		return ivec2(-1, -1);
	//Piece = 9 * x + 3 * y + z
	temp = int((coor[0] + 2.7f) / 1.8f);
	temp = (temp == 3) ? 2 : temp;
	result += temp * 9;
	temp = int((coor[1] + 2.7f) / 1.8f);
	temp = (temp == 3) ? 2 : temp;
	result += temp * 3; 
	temp = int((coor[2] + 2.7f) / 1.8f);
	temp = (temp == 3) ? 2 : temp;
	result += temp;
	//face x = 0, y = 1, z = 2
	if (abs(coor[1]) > abs(coor[0]))
		face = 1;
	if (abs(coor[2]) > abs(coor[face]))
		face = 2;
	return ivec2(result, face);
}

void swipeCallback(GLFWwindow *window, double xpos, double ypos){
	int x = int(xpos);
	// y axis is up-side down
	int y = int(h - ypos);
	float z;
	//Keep rotating the camera even when entering tweak bar zone
	if (third_person_mode || !TwEventMousePosGLFW(int(xpos), int(ypos))){
		if (mouseClicked){
			if (evMapping && HDR)
			{
				mainRT->bindFB();
				glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);
				glBindFramebuffer(GL_FRAMEBUFFER, 0);
			}
			else{
				glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);
			}
			//Find coordinates in world coordinates
			vec3 model = unProject(vec3(float(x), float(y), z), viewMatrix * modelMatrix, perspectiveMatrix, vec4(0.0f, 0.0f, w, h));
			ivec2 pieceInfo = findPiece(model);
			//If the cursor is outside of the rubik, change to third person camera view
			if (pieceInfo[0] == -1 || third_person_mode){
				third_person_mode = true;
				thirdPersonCallback(window, xpos, ypos);
			}
			else{
				if (lastPiece == -1){
					lastPiece = pieceInfo[0];
				}
				else{
					if (pieceInfo[0] != lastPiece && lastPiece != -1 && !swiping){
						direction = 0;
						for (int i = 0; i < 9; i++){
							for (int j = 0; j < 8; j++){
								if (lastPiece == swipe[i][j] && pieceInfo[0] == swipe[i][(j + 1) % 8] && (pieceInfo[1] == face[i][0] || pieceInfo[1] == face[i][1])){
									direction = 1.0f;
								}
								if (pieceInfo[0] == swipe[i][j] && lastPiece == swipe[i][(j + 1) % 8] && (pieceInfo[1] == face[i][0] || pieceInfo[1] == face[i][1])){
									direction = -1.0f;
								}
								if (direction != 0.0f){
									startAnimation = lastTime = glfwGetTime();
									swiping = true;
									swipePlane = i;
									totalAngle = 0.0;
									mouseClicked = false;
									break;
								}
							}
							if (direction != 0.0f)
								break;
						}
					}
				}
			}
		}
	}
	//Clear swiping data when entering tweak bar zone
	else{
		if (TwEventMousePosGLFW(int(xpos), int(ypos))){
			mouseClicked = false;
			lastPiece = -1;
		}
	}
}

void mousebuttonCallback(GLFWwindow* window, int button, int action, int mods){
	if (!TwEventMouseButtonGLFW(button, action)){
		if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
			mouseClicked = true;
		}
		if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE){
			mouseClicked = false;
			mouseX = mouseY = 0.0;
			third_person_mode = false;
		}
	}
}

void thirdPersonCallback(GLFWwindow *window, double xpos, double ypos){
	if (mouseClicked){
		if (mouseX == 0.0 && mouseY == 0.0){
			mouseX = xpos;
			mouseY = ypos;
		}
		float deltaX, deltaY;
		deltaX = float((xpos - mouseX) / w);
		deltaY = float((ypos - mouseY) / h);
		mouseX = xpos;
		mouseY = ypos;

		rotationAxis2 = vec3(rotate(mat4(1.0f), -deltaX * 2 * PI, rotationAxis1) * vec4(rotationAxis2, 1.0f));
		viewMatrix = rotate(viewMatrix, deltaX * 2 * PI, rotationAxis1);
		eye = vec3(rotate(mat4(1.0f), -deltaX * 2 * PI, rotationAxis1) * vec4(eye, 1.0f));

		rotationAxis1 = vec3(rotate(mat4(1.0f), -deltaY * 2 * PI, rotationAxis2) * vec4(rotationAxis1, 1.0f));
		viewMatrix = rotate(viewMatrix, deltaY * 2 * PI, rotationAxis2);
		eye = vec3(rotate(mat4(1.0f), -deltaY * 2 * PI, rotationAxis2) * vec4(eye, 1.0f));

		transposeInversedMatrix = glm::transpose(glm::inverse(modelMatrix));
		MVPMatrix = perspectiveMatrix * viewMatrix * modelMatrix;
	}
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset){
	float zoomFactor = 1;
	if (yoffset < 0.0){
		zoomFactor = 1.0f / (1.0f - float(yoffset) * 0.05f);
	}
	else{
		zoomFactor += 0.05f * float(yoffset);
	}
	modelMatrix = glm::scale(modelMatrix, glm::vec3(zoomFactor, zoomFactor, zoomFactor));
	MVPMatrix = perspectiveMatrix * viewMatrix * modelMatrix;
}

void framebufferCallback(GLFWwindow *window, int width, int height){
	float aspect = float(w * 1.0f / h);
	w = width;
	h = height;
	perspectiveMatrix = perspective(45.0f, aspect, 0.1f, 100.0f);
	MVPMatrix = perspectiveMatrix * viewMatrix * modelMatrix;
	glViewport(0, 0, GLsizei(w), GLsizei(h));
	mainRT->resizeFB(width, height);
	TwWindowSize(w, h);
}

void TW_CALL scumbleCallback(void *clientData)
{
	scrumble = true;
}

void TW_CALL setEVMappingCB(const void *value, void *clientData){
	(void)clientData;
	evMapping = *(const bool *)value;
	if (evMapping){
		TwDefine(" TweakBar/Mode readwrite ");
		TwAddVarRW(bar, "hdr", TW_TYPE_BOOLCPP, &HDR, " label='HDR Rendering' group='Environment mapping'");
		TwAddVarRW(bar, "exposure", TW_TYPE_FLOAT, &exposure, " label='Exposure' min=1.0f max=5.0f step=0.01f group='Environment mapping'");
		if (renderMode){
			TwAddVarRW(bar, "factor", TW_TYPE_FLOAT, &factor, " label='Reflection/Refraction factor' min=0.01f max=1.0f step=0.01 group='Environment mapping'");
		}
		if (renderMode == 2){
			TwAddVarRW(bar, "eta", TW_TYPE_FLOAT, &eta, " label='eta' min=0.01 max=1.0f step=0.01 group='Environment mapping'");
		}

	}
	else{
		TwDefine(" TweakBar/Mode readonly ");
		TwRemoveVar(bar, "hdr");
		TwRemoveVar(bar, "exposure");
		if (renderMode){
			TwRemoveVar(bar, "factor");
		}
		if (renderMode == 2){
			TwRemoveVar(bar, "eta");
		}
	}
}

void TW_CALL getEVMappingCB(void *value, void *clientData)
{
	(void)clientData; // unused
	*(bool *)value = evMapping;
}

void TW_CALL setModeCB(const void *value, void *clientData){
	(void)clientData;
	int oldRenderMode = renderMode;
	renderMode = *(const int *)value;
	if (renderMode && !oldRenderMode){
		TwAddVarRW(bar, "factor", TW_TYPE_FLOAT, &factor, " label='Reflection/Refraction factor' min=0.01 max=1.0f step=0.01 group='Environment mapping'");
	}
	else{
		if (renderMode == 0 && oldRenderMode)
			TwRemoveVar(bar, "factor");
	}

	if (renderMode == 2){
		TwAddVarRW(bar, "eta", TW_TYPE_FLOAT, &eta, " label='eta' min=0.01 max=1.0f step=0.01 group='Environment mapping'");
	}
	else{
		if (oldRenderMode == 2)
			TwRemoveVar(bar, "eta");
	}
}

void TW_CALL getModeCB(void *value, void *clientData)
{
	(void)clientData; // unused
	*(int *)value = renderMode;
}

int main(){
	GLFWwindow *window;
	w = 1280;
	h = 720;
	if (!glfwInit()){
		cout << "can't initialize GLFW";
		exit(EXIT_FAILURE);
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(w, h, "Rubik", NULL, NULL);
	glfwMakeContextCurrent(window);
	if (gl3wInit()){
		cout << "can't initialize gl3w";
		exit(EXIT_FAILURE);
	}
	init();
	TwInit(TW_OPENGL_CORE, NULL);
	bar = TwNewBar("TweakBar");
	TwDefine(" TweakBar size='300 180' ");
	TwAddButton(bar, "Scramble", scumbleCallback, NULL, NULL);
	
	TwAddSeparator(bar, "", NULL);
	TwAddVarCB(bar, "em", TW_TYPE_BOOLCPP, setEVMappingCB, getEVMappingCB, NULL, "label='Environment mapping' group='Environment mapping'");
	TwEnumVal modeEV[4] = { { 0, "None" }, { 1, "Reflection" }, { 2, "Refraction" }, { 3, "Fresnel" } };
	TwType modes = TwDefineEnum("Mode", modeEV, 4);
	TwAddVarCB(bar, "Mode", modes, setModeCB, getModeCB, NULL, "group='Environment mapping'");
	TwWindowSize(w, h);
	TwDefine(" TweakBar/'Environment mapping' opened = false ");
	TwDefine(" TweakBar/Mode readonly ");
	glfwSetMouseButtonCallback(window, mousebuttonCallback);
	glfwSetCursorPosCallback(window, swipeCallback);
	glfwSetScrollCallback(window, scrollCallback);
	glfwSetFramebufferSizeCallback(window, framebufferCallback);

	while (!glfwWindowShouldClose(window)){
		display();
		TwDraw();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	TwTerminate();
	glfwTerminate();
	return 0;
}