#pragma once
#include <tiny_obj_loader.h>
#include <glm/glm.hpp>
#include <vector>

//Merge shapes and material of an obj file
//We actually don't use any material files
class ObjPiece
{
public:
	ObjPiece();
	ObjPiece(const char* filename);
	ObjPiece(const ObjPiece& clone);
	ObjPiece& operator=(const ObjPiece& clone);
	~ObjPiece();
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	glm::vec4 color;
};