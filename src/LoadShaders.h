/*This is just a temporary fix for Intel Sandy Bride OpenGL 3.0 and OpenGL 3.3*/

#ifndef LOADSHADERS_H_INCLUDED
#define LOADSHADERS_H_INCLUDED
#include <string>
#include <vector>
#include <GL/gl3w.h>

using namespace std;

void format(string &input, vector<int> &location, vector<string> &name);
GLuint LoadShaders(const char* vertexShader, const char* fragmentShader);

#endif // LOADSHADERS_H_INCLUDED
