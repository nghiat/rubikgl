#include "pieces.h"
#include <vector>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;
using namespace std;

ObjPiece rubikCenter;
ObjPiece rubikCorner;
ObjPiece rubikEdge;
ObjPiece rubikSide;
ObjPiece rubikSticker;

ObjPiece center[CENTERNUM];
ObjPiece corner[CORNERNUM];
ObjPiece edge[EDGENUM];
ObjPiece side[SIDENUM];
ObjPiece stickers[TOTALPIECES * 2];
FullPiece pieces[TOTALPIECES];

int swipe[9][9] = {
	{ 0, 1, 2, 5, 8, 7, 6, 3, 4 },
	{ 9, 10, 11, 14, 17, 16, 15, 12, 13 },
	{ 18, 19, 20, 23, 26, 25, 24, 21, 22 },
	{ 0, 1, 2, 11, 20, 19, 18, 9, 10 },
	{ 3, 4, 5, 14, 23, 22, 21, 12, 13 },
	{ 6, 7, 8, 17, 26, 25, 24, 15, 16 },
	{ 0, 3, 6, 15, 24, 21, 18, 9, 12 },
	{ 1, 4, 7, 16, 25, 22, 19, 10, 13 },
	{ 2, 5, 8, 17, 26, 23, 20, 11, 14 },
};

vec3 axis[9] = {
	vec3(-1.0f, 0.0f, 0.0f),
	vec3(-1.0f, 0.0f, 0.0f),
	vec3(-1.0f, 0.0f, 0.0f),
	vec3(0.0f, 1.0f, 0.0f),
	vec3(0.0f, 1.0f, 0.0f),
	vec3(0.0f, 1.0f, 0.0f),
	vec3(0.0f, 0.0f, -1.0f),
	vec3(0.0f, 0.0f, -1.0f),
	vec3(0.0f, 0.0f, -1.0f),
};

int face[9][2] = {
	1, 2,
	1, 2,
	1, 2,
	0, 2,
	0, 2,
	0, 2, 
	0, 1,
	0, 1, 
	0, 1,
};
void loadBasePieces(){
	rubikCenter = ObjPiece("assets/rubik_center.obj");
	rubikCorner = ObjPiece("assets/rubik_corner.obj");
	rubikEdge = ObjPiece("assets/rubik_edge.obj");
	rubikSide = ObjPiece("assets/rubik_side.obj");
	rubikSticker = ObjPiece("assets/rubik_sticker.obj");
}

void loadCenters(){
	center[0] = rubikCenter;
}

void loadCorners(){
	ObjPiece tempPiece = rubikCorner;
	corner[0] = transform(tempPiece, vec3(1.78f, 1.78f, 1.78f), 0.0f, vec3(0.0f, 1.0f, 0.0f));
	corner[1] = transform(corner[0], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	corner[2] = transform(corner[1], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	corner[3] = transform(corner[2], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	corner[4] = transform(corner[0], vec3(0.0f), PI / 2.0f, vec3(1.0f, 0.0f, 0.0f));
	corner[5] = transform(corner[1], vec3(0.0f), PI / 2.0f, vec3(-1.0f, 0.0f, 0.0f));
	corner[6] = transform(corner[2], vec3(0.0f), PI / 2.0f, vec3(-1.0f, 0.0f, 0.0f));
	corner[7] = transform(corner[3], vec3(0.0f), PI / 2.0f, vec3(1.0f, 0.0f, 0.0f));
}

void loadEdges(){
	ObjPiece tempPiece = rubikEdge;
	edge[0] = transform(tempPiece, vec3(0.0f, 1.78f, 1.78f), 0.0, vec3(0.0f, 1.0f, 0.0f));
	edge[1] = transform(edge[0], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	edge[2] = transform(edge[1], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	edge[3] = transform(edge[2], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	edge[4] = transform(edge[0], vec3(0.0f, 0.0f, 0.0f), PI / 2, vec3(0.0f, 0.0f, 1.0f));
	edge[5] = transform(edge[4], vec3(0.0f, 0.0f, 0.0f), PI / 2, vec3(0.0f, 1.0f, 0.0f));
	edge[6] = transform(edge[5], vec3(0.0f, 0.0f, 0.0f), PI / 2, vec3(0.0f, 1.0f, 0.0f));
	edge[7] = transform(edge[6], vec3(0.0f, 0.0f, 0.0f), PI / 2, vec3(0.0f, 1.0f, 0.0f));
	edge[8] = transform(edge[0], vec3(0.0f), PI / 2.0f, vec3(1.0f, 0.0f, 0.0f));
	edge[9] = transform(edge[8], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	edge[10] = transform(edge[9], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	edge[11] = transform(edge[10], vec3(0.0f), PI / 2.0f, vec3(0.0f, 1.0f, 0.0f));
}

void loadSides(){
	ObjPiece tempPiece = rubikSide;
	//Don't have time to find the bug
	//side[0] = transform(tempPiece, vec3(0.0f, 0.0f, 1.78f), -PI / 2, vec3(1.0f, 0.0f, 0.0f));
	side[0] = transform(tempPiece, vec3(0.0f, 0.0f, 0.0f), -PI / 2, vec3(1.0f, 0.0f, 0.0f));
	side[0] = transform(side[0], vec3(0.0f, 1.78f, 0.0f), 0.0f, vec3(1.0f, 0.0f, 0.0f));
	side[1] = transform(tempPiece, vec3(0.0f, 0.0f, 1.78f), 0.0, vec3(0.0f, 1.0f, 0.0f));
	side[2] = transform(side[1], vec3(0.0f), PI / 2, vec3(0.0f, 1.0f, 0.0f));
	side[3] = transform(side[2], vec3(0.0f), PI / 2, vec3(0.0f, 1.0f, 0.0f));
	side[4] = transform(side[3], vec3(0.0f), PI / 2, vec3(0.0f, 1.0f, 0.0f));
	side[5] = transform(side[0], vec3(0.0f), PI, vec3(1.0f, 0.0f, 0.0f));
}

void loadStickers(){
	ObjPiece temp = rubikSticker;
	stickers[0] = transform(temp, vec3(-1.78f, -1.78f, 1.8f), 0.0f, vec3(1.0f, 0.0f, 0.0f));
	stickers[0].color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	stickers[1] = transform(stickers[0], vec3(0.0f, 1.78f, 0.0f), 0.0f, vec3(1.0f));
	stickers[2] = transform(stickers[1], vec3(0.0f, 1.78f, 0.0f), 0.0f, vec3(1.0f));
	for (int i = 1; i < 3; i++){
		for (int j = 0; j < 3; j++){
			stickers[3 * i + j] = transform(stickers[3 * i + j - 3], vec3(1.78f, 0.0f, 0.0f), 0.0f, vec3(1.0f));
		}
	}

	for (int i = 9; i < 18; i++){
		stickers[i] = transform(stickers[i - 9], vec3(0.0f), PI / 2, vec3(1.0f, 0.0f, 0.0f));
		stickers[i].color = vec4(1.0f, 0.823f, 0.0f, 0.98f);
	}

	for (int i = 18; i < 27; i++){
		stickers[i] = transform(stickers[i - 9], vec3(0.0f), PI / 2, vec3(1.0f, 0.0f, 0.0f));
		stickers[i].color = vec4(0.0f, 0.2f, 0.451f, 0.98f);
	}

	for (int i = 27; i < 36; i++){
		stickers[i] = transform(stickers[i - 9], vec3(0.0f), PI / 2, vec3(1.0f, 0.0f, 0.0f));
		stickers[i].color = vec4(1.0f, 0.0f, 0.0f, 0.98f);
	}

	for (int i = 0; i < 9; i++){
		stickers[36 + i] = transform(stickers[i], vec3(0.0f), PI / 2, vec3(0.0f, 1.0f, 0.0f));
		stickers[36 + i].color = vec4(0.0f, 0.451f, 0.184f, 0.98f);
	}
	for (int i = 0; i < 9; i++){
		stickers[45 + i] = transform(stickers[i], vec3(0.0f), -PI / 2, vec3(0.0f, 1.0f, 0.0f));
		stickers[45 + i].color = vec4(1.0f, 0.275f, 0.0f, 0.98f);
	}

}

void loadPieces(){
	loadBasePieces();
	loadCenters();
	loadCorners();
	loadEdges();
	loadSides(); 
	loadStickers();

	pieces[0].piece = corner[6];
	pieces[0].stickers.push_back(stickers[9]);
	pieces[0].stickers.push_back(stickers[45]);
	pieces[0].stickers.push_back(stickers[20]);

	pieces[1].piece = edge[11];
	pieces[1].stickers.push_back(stickers[10]);
	pieces[1].stickers.push_back(stickers[48]);

	pieces[2].piece = corner[7];
	pieces[2].stickers.push_back(stickers[11]);
	pieces[2].stickers.push_back(stickers[0]);
	pieces[2].stickers.push_back(stickers[51]);

	pieces[3].piece = edge[7];
	pieces[3].stickers.push_back(stickers[19]);
	pieces[3].stickers.push_back(stickers[46]);

	pieces[4].piece = side[4];
	pieces[4].stickers.push_back(stickers[49]);

	pieces[5].piece = edge[4];
	pieces[5].stickers.push_back(stickers[1]);
	pieces[5].stickers.push_back(stickers[52]);

	pieces[6].piece = corner[2];
	pieces[6].stickers.push_back(stickers[18]);
	pieces[6].stickers.push_back(stickers[29]);
	pieces[6].stickers.push_back(stickers[47]);

	pieces[7].piece = edge[3];
	pieces[7].stickers.push_back(stickers[28]);
	pieces[7].stickers.push_back(stickers[50]);

	pieces[8].piece = corner[3];
	pieces[8].stickers.push_back(stickers[2]);
	pieces[8].stickers.push_back(stickers[27]);
	pieces[8].stickers.push_back(stickers[53]);


	pieces[9].piece = edge[10];
	pieces[9].stickers.push_back(stickers[12]);
	pieces[9].stickers.push_back(stickers[23]);

	pieces[10].piece = side[5];
	pieces[10].stickers.push_back(stickers[13]);

	pieces[11].piece = edge[8];
	pieces[11].stickers.push_back(stickers[3]);
	pieces[11].stickers.push_back(stickers[14]);

	pieces[12].piece = side[3];
	pieces[12].stickers.push_back(stickers[22]);

	pieces[13].piece = center[0];

	pieces[14].piece = side[1];	 
	pieces[14].stickers.push_back(stickers[4]);

	pieces[15].piece = edge[2];	 
	pieces[15].stickers.push_back(stickers[21]);
	pieces[15].stickers.push_back(stickers[32]);

	pieces[16].piece = side[0];	 
	pieces[16].stickers.push_back(stickers[31]);

	pieces[17].piece = edge[0];	
	pieces[17].stickers.push_back(stickers[5]);
	pieces[17].stickers.push_back(stickers[30]);


	pieces[18].piece = corner[5];
	pieces[18].stickers.push_back(stickers[15]);
	pieces[18].stickers.push_back(stickers[26]);
	pieces[18].stickers.push_back(stickers[42]);

	pieces[19].piece = edge[9];
	pieces[19].stickers.push_back(stickers[16]);
	pieces[19].stickers.push_back(stickers[39]);

	pieces[20].piece = corner[4];
	pieces[20].stickers.push_back(stickers[17]);
	pieces[20].stickers.push_back(stickers[36]);
	pieces[20].stickers.push_back(stickers[6]);

	pieces[21].piece = edge[6];
	pieces[21].stickers.push_back(stickers[25]);
	pieces[21].stickers.push_back(stickers[43]);

	pieces[22].piece = side[2];
	pieces[22].stickers.push_back(stickers[40]);

	pieces[23].piece = edge[5];
	pieces[23].stickers.push_back(stickers[7]);
	pieces[23].stickers.push_back(stickers[37]);

	pieces[24].piece = corner[1];
	pieces[24].stickers.push_back(stickers[24]);
	pieces[24].stickers.push_back(stickers[35]);
	pieces[24].stickers.push_back(stickers[44]);

	pieces[25].piece = edge[1];
	pieces[25].stickers.push_back(stickers[34]);
	pieces[25].stickers.push_back(stickers[41]);

	pieces[26].piece = corner[0];
	pieces[26].stickers.push_back(stickers[38]);
	pieces[26].stickers.push_back(stickers[33]);
	pieces[26].stickers.push_back(stickers[8]);
}

ObjPiece transform(const ObjPiece& originalPiece, glm::vec3 translateVector, float angle, glm::vec3 v){
	ObjPiece newPiece = originalPiece;
	mat4 transformMatrix = mat4(1.0f);
	mat4 tranformNormalMatrix = mat4(1.0f);
	transformMatrix = rotate(transformMatrix, angle, v);
	if (transformMatrix != mat4(0.0f))
		tranformNormalMatrix = transpose(inverse(transformMatrix));
	transformMatrix = translate(transformMatrix, translateVector);
	int shapeNum = newPiece.shapes.size();
	for (int i = 0; i < shapeNum; i++){
		int positionNum = newPiece.shapes[i].mesh.positions.size();
		for (int j = 0; j < positionNum / 3; j++){
			//Transform both vertices and normal vectors
			vec3 vertex = vec3(newPiece.shapes[i].mesh.positions[3 * j], newPiece.shapes[i].mesh.positions[3 * j + 1], newPiece.shapes[i].mesh.positions[3 * j + 2]);
			vec3 normalVector = vec3(newPiece.shapes[i].mesh.normals[3 * j], newPiece.shapes[i].mesh.normals[3 * j + 1], newPiece.shapes[i].mesh.normals[3 * j + 2]);
			vertex = vec3(transformMatrix * vec4(vertex, 1.0f));
			normalVector = vec3(tranformNormalMatrix * vec4(normalVector, 0.0f));
			newPiece.shapes[i].mesh.positions[3 * j] = vertex[0];
			newPiece.shapes[i].mesh.positions[3 * j + 1] = vertex[1];
			newPiece.shapes[i].mesh.positions[3 * j + 2] = vertex[2];
			newPiece.shapes[i].mesh.normals[3 * j] = normalVector[0];
			newPiece.shapes[i].mesh.normals[3 * j + 1] = normalVector[1];
			newPiece.shapes[i].mesh.normals[3 * j + 2] = normalVector[2];
		}
	}

	return newPiece;
}